# Templates

## Overview

This project contains the templates used as an example in the `gitlab_demo` Project to demo Multi-Project Pipeline scenarios.

## Project Structure

- `jobs/` - Contain job templates for reusable Jobs that can be found in the workflows under `workflows/`, or other Projects if applicable.
  - `eslint_template.yml` - Templated / muted job for eslint (JavaScript code)
  - `pylint_template.yml` - Templated / muted job for pylint (Python code)
  - `pytest_template.yml` - Templated /muted job for Pytest & testing Python code.
- `workflows/` - Contain reusable files of certain workflows that can be used across multiple Projects.
  - `linting_pipeline.yml` - The Linting Pipeline workflow for linting a Project that lints for JavaScript and Python
  - `testing_pipeline.yml` - The Testing Pipeline workflow for testing a Project's Python code.

```txt
__jobs/
|__eslint_template.yml
|__pylint_template.yml
|__pytest_template.yml
__workflows/
|__linting_pipeline.yml
```
